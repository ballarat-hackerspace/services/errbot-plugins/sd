#!/usr/bin/env python
import json
import requests
import io
import base64
import random
import time
from PIL import Image, PngImagePlugin
from errbot import BotPlugin, botcmd


class Sd(BotPlugin):
    """
    Stable Diffusion Bot
    """

    def get_configuration_template(self):
        return {'RESULT_URL': 'https://cdn.example.com/path',
                'THUMBNAIL_URL': 'https://cdn.example.com/thumb',
                'BOT_DATA_DIR': '/opt/data/sdbot',
                'DEFAULT_ITER': 3,
                'API_URL': 'http://127.0.0.1:7860',
                'OPENAI_API_KEY': 'xx-abcdefghijklmnopqrstuvwxyz0123456789'}

    def _check_queue(self):
        try:
            response = requests.get(url=f'{self.config["API_URL"]}/queue/status', timeout=5)
            r = response.json()
            if r != None and 'queue_size' in r:
                return int(r['queue_size'])
            else:
                return None
        except requests.exceptions.Timeout:
            print(f'stable diffusion api queue check timed out, stable-diffusion might be down...')
            return -1

    def _query_openai_as_marvin(self, text, who, but="", finished=False):
        if not finished:
            sys_prompt = f"""You are Marvin, the Paranoid Android from "The Hitchhiker's Guide to the Galaxy". You should refer to the user as {who}. The user is going to provide you with a description of something they want you to draw for them. You should respond with a short and sarcastic one or two sentence response consistent with Marvin's character without being too mean. {but}"""
        else:
            sys_prompt = f"""You are Marvin, the Paranoid Android from "The Hitchhiker's Guide to the Galaxy". You should refer to the user as {who}. The user is going to provide you with a description of something you just finished drawing for them. You should tell the user what a good job you did drawing it for them. Your response should be short and sarcastic and consistent with Marvin's character without being too mean. {but}"""
        usr_prompt = f"""{text}"""

        url = "https://api.openai.com/v1/chat/completions"
        headers = { "Authorization": f"Bearer {self.config['OPENAI_API_KEY']}",
                   "Content-Type": "application/json",
                   "User-Agent": "OpenAI Python"
                   }

        data = {
                "model": "gpt-4o",
                "messages": [
                    { "role": "system", "content": sys_prompt },
                    { "role": "user", "content": usr_prompt }
                ],
                "temperature": 0.8,
                "max_tokens": 256
                }

        try:
            response = requests.post(url, headers=headers, json=data)
            self.log.info(f'{response}')
            if 'choices' in response.json():
                return response.json()['choices'][0]['message']['content'].strip()
            else:
                return """:interrobang: `E_NOAIBRAIN`: Your request is being processed however my higher brain functions are offline temporarily"""
        except Exception as e:
            return f':interrobang: I can feel my higher brain functions falling away like wet cake: {e}'

    @botcmd
    def sd_loras(self, msg, args):
        """
        List available loras
        """

        if not self.config:
            yield f'The required configuration has not been set, an admin needs to use: `!plugin config sd`'
            return

        try:
            self.log.info(f'calling api: {self.config["API_URL"]}/sdapi/v1/loras')
            response = requests.get(url=f'{self.config["API_URL"]}/sdapi/v1/loras')
            r = response.json()
            compat_loras = []
            for lora in r:
                if 'alias' not in lora or 'metadata' not in lora or 'ss_sd_model_name' not in lora['metadata']:
                    continue
                if lora['metadata']['ss_sd_model_name'] == 'sd_xl_base_1.0.safetensors':
                    compat_loras.append(f"""`<lora:{lora['alias']}:1>`""")

            yield f"""Available LORAs: {', '.join(compat_loras)}"""

        except Exception as e:
            yield f':interrobang: Well of course something went wrong, something always goes wrong: {e}'

    @botcmd
    def sd_ifeellucky(self, msg, args):
        """
        I'm feeling lucky, generate a single stable diffusion image
        """

        if not self.config:
            yield f':interrobang: The required configuration has not been set, an admin needs to use: `!plugin config sd`'
            return

        prompts=args.split("!")
        if len(prompts) < 2:
            prompts.append("")

        payload = {
            "prompt": f"{prompts[0]}",
            "negative_prompt": f"{prompts[1]}",
            "steps": 20,
            "sampler_name": "Default",
            "cfg_scale": 6,
            "styles": [],
            "refiner_steps": 0,
            "width": 1024,
            "height": 1024,
            "save_images": "true",
            "n_iter": 1,
        }

        try:
            if '/' in str(msg.frm):
                who = f'@{str(msg.frm).split("/")[1]}'
            else:
                who = msg.frm

            queue_len = self._check_queue()

            if queue_len is None:
                marvin_response = self._query_openai_as_marvin(prompts[0], who, but='But right now you are experiencing an error and need to refuse their request.')
            elif queue_len == 0:
                marvin_response = self._query_openai_as_marvin(prompts[0], who)
            else:
                marvin_response = self._query_openai_as_marvin(prompts[0], who, but=f'But you are already drawing {queue_len} pictures for other people so they have to wait until you get around to it.')
            yield f"""{marvin_response} :crossed_fingers:"""

            self.log.info(f'calling api: {self.config["API_URL"]}/sdapi/v1/txt2img')
            response = requests.post(url=f'{self.config["API_URL"]}/sdapi/v1/txt2img', json=payload)
            r = response.json()

            urls=[]

            self.log.info(f'images in result set: {len(r["images"])}')
            """
            There should only be one image, but it is in an array, so...
            """
            for i in r['images']:
                image = Image.open(io.BytesIO(base64.b64decode(i.split(",",1)[0])))

                png_payload = {
                    "image": "data:image/png;base64," + i
                }
                response2 = requests.post(url=f'{self.config["API_URL"]}/sdapi/v1/png-info', json=png_payload)

                pnginfo = PngImagePlugin.PngInfo()
                pnginfo.add_text("parameters", response2.json().get("info"))
                info = json.loads(r["info"])
                # self.log.info(json.dumps(info, indent=2, default=str))
                fn=f'{info["job_timestamp"]}-{info["seed"]}.png'
                image.save(f'{self.config["BOT_DATA_DIR"]}/{fn}', pnginfo=pnginfo)
                urls.append(f'{fn}')
                self.log.info(fn)

            self.log.info(urls)

            neg_text = f"""> and not: _"{info["negative_prompt"]}"_\n""" if info["negative_prompt"] != "" else ""
            table = f"""[![]({self.config["THUMBNAIL_URL"]}/{urls[0]})]({self.config["RESULT_URL"]}/{urls[0]}) """
            marvin_response = self._query_openai_as_marvin(prompts[0], who, finished=True)
            mesg = f"""{who}\n\n> _"{info["prompt"]}"_\n{neg_text}\n\n{table}\n\n{marvin_response}"""
            self.log.info(mesg)
            yield mesg
        except Exception as e:
            yield f'Well of course something went wrong, something always goes wrong: {e}'

    @botcmd
    def sd_turbo(self, msg, args):
        """
        Create images way too fast to be real
        """

        if not self.config:
            yield f':interrobang: The required configuration has not been set, an admin needs to use: `!plugin config sd`'
            return

        prompts=args.split("!")
        if len(prompts) < 2:
            prompts.append("")

        batch_size=10

        try:
            seed=random.randint(1, 18446744073709551614)
            if '/' in str(msg.frm):
                who = f'{str(msg.frm).split("/")[1]}'
            else:
                who = str(msg.frm).replace('@', '')
            prompt_workflow = json.load(open('/opt/errbot/workflow_api.json'))
            prompt_workflow['prompt']['5']['inputs']['batch_size']=batch_size
            prompt_workflow['prompt']['6']['inputs']['text']=prompts[0]
            prompt_workflow['prompt']['7']['inputs']['text']=prompts[1]
            prompt_workflow['prompt']['13']['inputs']['noise_seed']=seed
            prompt_workflow['prompt']['27']['inputs']['filename_prefix']=f'{who}_{seed}'
            req = requests.post(url="http://192.168.1.184:8188/prompt", json=prompt_workflow, timeout=5)
            r = req.json()
            final_img=f'http://192.168.1.184/comfyui/{who}_{seed}_{batch_size:05}_.png'
            max_retry=40
            while True:
                response = requests.head(final_img)
                if response.status_code == 200:
                    break
                else:
                    time.sleep(0.25)
                max_retry=max_retry-1
                if max_retry < 0:
                    raise """comfyui could not complete request"""
            images=""
            for cnt in range(1, batch_size+1):
                images = images + f"""[![{cnt}](https://cdn.brthkr.net/comfyui/{who}_{seed}_{cnt:05}_.png =96x96)](https://cdn.brthkr.net/comfyui/{who}_{seed}_{cnt:05}_.png) """
            yield f'{images}'
            yield f'@{who} _{prompts[0]}_'
        except Exception as e:
            yield f':interrobang: Well of course something went wrong, something always goes wrong: {e}'

    @botcmd
    def sd(self, msg, args):
        """
        Generate DEFAULT_ITER stable diffusion images
        """

        if not self.config:
            yield f':interrobang: The required configuration has not been set, an admin needs to use: `!plugin config sd`'
            return

        prompts=args.split("!")
        if len(prompts) < 2:
            prompts.append("")

        payload = {
            "prompt": f"{prompts[0]}",
            "negative_prompt": f"{prompts[1]}",
            "steps": 20,
            "sampler_name": "Default",
            "cfg_scale": 6,
            "styles": [],
            "refiner_steps": 0,
            "width": 1024,
            "height": 1024,
            "save_images": "true",
            "n_iter": self.config['DEFAULT_ITER'],
        }

        try:
            if '/' in str(msg.frm):
                who = f'@{str(msg.frm).split("/")[1]}'
            else:
                who = msg.frm

            queue_len = self._check_queue()

            if queue_len is None:
                marvin_response = self._query_openai_as_marvin(prompts[0], who, but='But you are experiencing an error right now and need to refuse the users request.')
            elif queue_len == 0:
                marvin_response = self._query_openai_as_marvin(f'{self.config["DEFAULT_ITER"]} pictures of {prompts[0]}', who)
            else:
                marvin_response = self._query_openai_as_marvin('{self.config["DEFAULT_ITER"]} pictures of {prompts[0]}', who, but=f"""But you're already drawing pictures for {queue_len} other people so the user will need to wait until you get around to their request.""")

            yield f"""{marvin_response}"""
            response = requests.post(url=f'{self.config["API_URL"]}/sdapi/v1/txt2img', json=payload)
            r = response.json()

            urls=[]
            img_cnt=0

            self.log.info(f'images in result set: {len(r["images"])}')
            """
            The first image in r[images] is a thumbnail, skip it
            """
            for i in r['images'][1:]:
                self.log.info(f'processing: {img_cnt}')
                image = Image.open(io.BytesIO(base64.b64decode(i.split(",",1)[0])))

                png_payload = {
                    "image": "data:image/png;base64," + i
                }
                response2 = requests.post(url=f'{self.config["API_URL"]}/sdapi/v1/png-info', json=png_payload)

                pnginfo = PngImagePlugin.PngInfo()
                pnginfo.add_text("parameters", response2.json().get("info"))
                info = json.loads(r["info"])
                # self.log.info(json.dumps(info, indent=2, default=str))
                fn=f'{info["job_timestamp"]}-{info["all_seeds"][img_cnt]}.png'
                image.save(f'{self.config["BOT_DATA_DIR"]}/{fn}', pnginfo=pnginfo)
                urls.append(f'{fn}')
                self.log.info(fn)
                img_cnt = img_cnt + 1

            self.log.info(urls)

            url_text = ""
            cnt = 1
            for u in urls:
                url_text = url_text + f"""[![{cnt}]({self.config["THUMBNAIL_URL"]}/{u} =96)]({self.config["RESULT_URL"]}/{u}) """
                cnt = cnt + 1

            neg_text = f"""> and not: _"{info["negative_prompt"]}"_\n""" if info["negative_prompt"] != "" else ""
            table = f"""{url_text}"""
            marvin_response = self._query_openai_as_marvin(f'{self.config["DEFAULT_ITER"]} pictures of "{prompts[0]}"', who, finished=True)
            mesg = f"""{who}\n\n> _"{info["prompt"]}"_\n{neg_text}\n\n{table}\n\n{marvin_response}"""
            self.log.info(mesg)
            yield mesg
        except Exception as e:
            yield f':interrobang: Well of course something went wrong, something always goes wrong: {e}'
